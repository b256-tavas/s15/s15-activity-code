let firstName = "John";
let lastName = "Smith";
let age = 30;

firstName = "First Name: " + firstName;
console.log(firstName);

lastName = "Last Name: " + lastName;
console.log(lastName);

age = "Age: " + age;
console.log(age);

hobbies = "Hobbies: ";
console.log(hobbies);

let hobby = ['swimming', 'mountain climbing', 'biking'];
console.log(hobby);

let address = "Work Address: ";
console.log(address);

let workAddress = {

	houseNumber: 32,
	street: "Washington",
	city: "Lincoln",
	state: "Nebraska"
}
console.log(workAddress);



/*


	1. Create variables to store to the following user details:


	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output. */


	let fullName = "Steve Rogers";
	console.log("My full name is: " + fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);
	
	console.log("My Friends are:")
	let friends = ["Tony","Bruce","Thor","Natasha","Clint","Nick"];
	console.log(friends);

	console.log("My Full Profile: ")
	
	let profile = {

		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false,

	}
	console.log(profile);

	let bestfriend = "Bucky Barnes";
	console.log("My bestfriend is: " + bestfriend);

	const lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);
